var sliders = $(document).find('.container-slider');
$(document).ready(initiateSliders);
var timer = setInterval(automatedSlide, 5000);

function initiateSliders() {
    for (var i = 0; i < sliders.length; i++) {
        slidesCount = sliders.eq(i).find('li').length / sliders.eq(i).attr('data-show-slides');
        imageSlideCount = parseInt(sliders.eq(i).attr('data-show-slides'));
        sliderHeight = sliders.eq(i).attr('data-slider-height');
        sliders.eq(i).find('.slider-image').css({
          height: sliderHeight,
        });
        sliders.eq(i).find('.slider').css({
            width: (100 * slidesCount) + '%',
        });
        sliders.eq(i).find('li').css({
            width: (100 / slidesCount) / imageSlideCount + '%'
        });
        if (sliders.eq(i).attr('data-buttons') == 'false') {
            sliders.eq(i).find('.controls-buttons').hide();
        }
        if (sliders.eq(i).attr('data-dots') == 'true') {
            for (var j = 0; j < slidesCount; j++) {
                sliders.eq(i).find('.controls-dots').append('<button type="button" class="circle" id="dot-control-' + j + '" data-dots-controls></button>');
            }
            sliders.eq(i).find('#dot-control-0').addClass('controls-dot-selected');
        }
    }
}


function automatedSlide() {
    for (var i = 0; i < sliders.length; i++) {
        if (sliders.eq(i).attr('data-autoplay') == 'true')
            nextSlide(sliders.eq(i));
    }
}

function resetTimer(parentSlider) {
    if (parentSlider.attr('data-autoplay') == 'true') {
        clearInterval(timer);
        timer = setInterval(automatedSlide, 5000);
    }
}

function nextSlide(parentSlider) {
    var id = parentSlider.attr('data-slider-id');
    var slidesCount = sliders.eq(id).find('li').length / parentSlider.attr('data-show-slides');
    var slideWidth = sliders.eq(id).find('li').width() * parentSlider.attr('data-show-slides');
    var value = parseInt(parentSlider.attr('data-current-slide')) + slideWidth;
    if (value > slideWidth * (slidesCount - 1)) {
        value = 0;
    }
    sliders.eq(id).find('.slider').animate({
        right: value
    }, 1000);
    parentSlider.attr('data-current-slide', value);
    if (parentSlider.attr('data-dots') == 'true') {
        var currentSlide = (parentSlider.attr('data-current-slide') / slideWidth);
        parentSlider.find('.circle').removeClass('controls-dot-selected');
        parentSlider.find('#dot-control-' + currentSlide).addClass('controls-dot-selected');
    }
}

function previousSlide(parentSlider) {
    var id = parentSlider.attr('data-slider-id');
    var slidesCount = sliders.eq(id).find('li').length / parentSlider.attr('data-show-slides');
    var slideWidth = sliders.eq(id).find('li').width() * parentSlider.attr('data-show-slides');
    var value = parseInt(parentSlider.attr('data-current-slide')) - slideWidth;
    if (value < 0) {
        value = slideWidth * (slidesCount - 1)
    }
    sliders.eq(id).find('.slider').animate({
        right: value
    }, 1000);
    sliders.eq(id).attr('data-current-slide', value);
    if (parentSlider.attr('data-dots') == 'true') {
        var currentSlide = (parentSlider.attr('data-current-slide') / slideWidth);
        parentSlider.find('.circle').removeClass('controls-dot-selected');
        parentSlider.find('#dot-control-' + currentSlide).addClass('controls-dot-selected');
    }
}

$("[data-next-slide]").click(function() {
    var parentSlider = $(this).parents('.container-slider');
    resetTimer(parentSlider);
    nextSlide(parentSlider);
    $(this).blur();
});

$("[data-prev-slide]").click(function() {
    var parentSlider = $(this).parents('.container-slider');
    resetTimer(parentSlider);
    previousSlide(parentSlider);
    $(this).blur();
});
$(document).on('click', '[data-dots-controls]', function() {
    var parentSlider = $(this).parents('.container-slider');
    parentSlider.find('.circle').removeClass('controls-dot-selected');
    var id = parentSlider.attr('data-slider-id');
    var slideWidth = sliders.eq(id).find('li').width() * parentSlider.attr('data-show-slides');
    var selectedSlide = $(this).attr('id').substr(-1);
    var value = selectedSlide * slideWidth;
    sliders.eq(id).find('.slider').animate({
        right: value
    }, 1000);
    sliders.eq(id).attr('data-current-slide', value);
    $(this).addClass('controls-dot-selected')
});
