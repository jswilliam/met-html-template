$('[data-comment]').click(function(event) {
    $(this).val("");
});
$('[data-comment]').keypress(function(e) {
    if (e.which == 13) {
        let comment = $('[data-comment]').val()
        console.log(comment);
        $('[all-comments]').prepend(
            `<div class="border"></div>
            <div class="comment row">
          <div class="img-div large-2 columns" style="background-image: url('img/face5.png')">
          </div>
          <div class="comment-div large-10 columns">
              <div class="row">
                  <h6>NICK DOE</h6>
                  <i class="light-gray fa fa-calendar-o" aria-hidden="true"></i><span class="news-date">Just now</span>
              </div>
              <p class="light-gray">${comment}</p>
          </div>
      </div>`
        )
        $(this).val("");
        return false;
    }
});

$('[data-dropdown]').click(function(event){
  if ($(this).parent().find('.dropdown-content').get(0) !== $('.dropdown-content.active').get(0)) {
    console.log("not equal");
    $('.dropdown-content.active').removeClass('active');
  }
  $(this).parent().find('.dropdown-content').toggleClass('active');
  return false;
})

$(document).click(function(){
  $('.dropdown-content').removeClass('active');
})

$('[data-dropdown-item]').click(function(event) {
  event.preventDefault();
  var parent = $(this).parents().eq(2);
  var text = $(this).text();
  parent.find('span').text(text);
  $('.dropdown-content').removeClass('active');
})
