# README #

### Slider component:###
```
#!html

<div class="container-slider" data-slider-id="1" data-current-slide="0" data-dots="true" data-buttons="true" data-autoplay="false" data-show-slides="1" data-slider-height="350px">
        <div class="slider">
            <li>
                <div class="slider-image" style="background-image: url('img/img1.jpg')"></div>
            </li>
            <li>
                <div class="slider-image" style="background-image: url('img/img2.jpg')"></div>
            </li>
            <li>
                <div class="slider-image" style="background-image: url('img/img3.jpg')"></div>
            </li>
        </div>
        <div class="clear"></div>
        <div class="controls">
            <center>
                <div class="controls-buttons">
                    <button type="button" class="button-prev" data-prev-slide><i class="fi-arrow-left"></i></button>
                    <button type="button" class="button-next" data-next-slide><i class="fi-arrow-right"></i></button>
                </div>
                <div class="controls-dots">
                </div>
            </center>
        </div>
    </div>
```
### Slider Data Attributes: ###
* **data-slider-id**: slider number in page (starts from 0)
* **data-current-slide**: 0 (leave default)
* **data-dots**: slider dots control (true/false)
* **data-buttons**: slider button control (true/false)
* **data-autoplay**: slider auto-play slides (true/false)
* **data-show-slides**: slider number of images per slide (default is 1)
* **data-slider-height**: height in pixels of slider images
